
PLAY [installLAMP] ***********************************************************************************************************************************************

TASK [Gathering Facts] *******************************************************************************************************************************************
ok: [20.246.7.228]

TASK [installLAMP : LAMP Install] ********************************************************************************************************************************
included: /mnt/c/Users/vinicius.alcantara/Documents/projetoTerraform-MBA/ubuntuServerProvisione/php8/ansible/installLAMP/roles/installLAMP/tasks/installLAMP.yml for 20.246.7.228

TASK [installLAMP : Apache Service Install] **********************************************************************************************************************
changed: [20.246.7.228]

TASK [installLAMP : Start Apache Service and Enable Start in Boot] ***********************************************************************************************
ok: [20.246.7.228]

TASK [installLAMP : Register Apache Version] *********************************************************************************************************************
changed: [20.246.7.228]

TASK [installLAMP : Dependencies Install] ************************************************************************************************************************
changed: [20.246.7.228]

TASK [installLAMP : Add Key Mariadb Repo] ************************************************************************************************************************
changed: [20.246.7.228]

TASK [installLAMP : Configure Mariadb Repo] **********************************************************************************************************************
changed: [20.246.7.228]

TASK [installLAMP : Update packages] *****************************************************************************************************************************
ok: [20.246.7.228]

TASK [installLAMP : Mariadb Service Install] *********************************************************************************************************************
changed: [20.246.7.228]

TASK [installLAMP : Start Mariadb Service and Enable Start in Boot] **********************************************************************************************
ok: [20.246.7.228]

TASK [installLAMP : Register Mariadb Version] ********************************************************************************************************************
changed: [20.246.7.228]

TASK [installLAMP : Configure PHP8 Repo] *************************************************************************************************************************
changed: [20.246.7.228]

TASK [installLAMP : Update packages] *****************************************************************************************************************************
changed: [20.246.7.228]

TASK [installLAMP : Install and Configure PHP8] ******************************************************************************************************************
changed: [20.246.7.228] => (item=php8.0)
ok: [20.246.7.228] => (item=libapache2-mod-php8.0)
ok: [20.246.7.228] => (item=php8.0-common)
changed: [20.246.7.228] => (item=php8.0-fpm)
changed: [20.246.7.228] => (item=php8.0-mysql)
changed: [20.246.7.228] => (item=php8.0-gmp)
changed: [20.246.7.228] => (item=php8.0-xml)
changed: [20.246.7.228] => (item=php8.0-xmlrpc)
changed: [20.246.7.228] => (item=php8.0-curl)
changed: [20.246.7.228] => (item=php8.0-mbstring)
changed: [20.246.7.228] => (item=php8.0-gd)
changed: [20.246.7.228] => (item=php8.0-dev)
changed: [20.246.7.228] => (item=php8.0-imap)
ok: [20.246.7.228] => (item=php8.0-opcache)
ok: [20.246.7.228] => (item=php8.0-readline)
changed: [20.246.7.228] => (item=php8.0-soap)
changed: [20.246.7.228] => (item=php8.0-zip)
changed: [20.246.7.228] => (item=php8.0-intl)
ok: [20.246.7.228] => (item=php8.0-cli)

TASK [installLAMP : Register PHP Version] ************************************************************************************************************************
changed: [20.246.7.228]

TASK [installLAMP : Print Apache Version] ************************************************************************************************************************
ok: [20.246.7.228] => {
    "changed": false,
    "msg": "Server version: Apache/2.4.52 (Ubuntu)\nServer built:   2023-01-23T18:34:42"
}

TASK [installLAMP : Print Mariadb Version] ***********************************************************************************************************************
ok: [20.246.7.228] => {
    "changed": false,
    "msg": "mysql  Ver 15.1 Distrib 10.8.7-MariaDB, for debian-linux-gnu (x86_64) using  EditLine wrapper"
}

TASK [installLAMP : Print PHP Version] ***************************************************************************************************************************
ok: [20.246.7.228] => {
    "changed": false,
    "msg": "PHP 8.0.28 (cli) (built: Feb 14 2023 18:33:29) ( NTS )\nCopyright (c) The PHP Group\nZend Engine v4.0.28, Copyright (c) Zend Technologies\n    with Zend OPcache v8.0.28, Copyright (c), by Zend Technologies"
}

TASK [installLAMP : Restart Apache Service] **********************************************************************************************************************
changed: [20.246.7.228]

PLAY RECAP *******************************************************************************************************************************************************
20.246.7.228              : ok=20   changed=12   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

